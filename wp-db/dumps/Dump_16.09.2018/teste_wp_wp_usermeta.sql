-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: teste_wp
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_usermeta`
--

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'nickname','Steve Han'),(2,1,'first_name',''),(3,1,'last_name',''),(4,1,'description','This is the best Event Ticket WordPress Theme of 2018. It has responsive, drag &amp; drop page builder, unlimited theme options. And made by Envato Elite Author - LeafColor.com'),(5,1,'rich_editing','true'),(6,1,'syntax_highlighting','true'),(7,1,'comment_shortcuts','false'),(8,1,'admin_color','fresh'),(9,1,'use_ssl','0'),(10,1,'show_admin_bar_front','true'),(11,1,'locale',''),(12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'),(13,1,'wp_user_level','10'),(14,1,'dismissed_wp_pointers','text_widget_custom_html,vc_pointers_backend_editor,wp496_privacy'),(15,1,'show_welcome_panel','0'),(16,1,'session_tokens','a:1:{s:64:\"825617039bb51c9d276f296d00a2e0c03680406238597c2a8caf109bb8fadc0f\";a:4:{s:10:\"expiration\";i:1537288802;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36\";s:5:\"login\";i:1537116002;}}'),(17,1,'wp_dashboard_quick_press_last_post_id','3967'),(18,1,'community-events-location','a:1:{s:2:\"ip\";s:2:\"::\";}'),(19,1,'_woocommerce_persistent_cart_1','a:1:{s:4:\"cart\";a:2:{s:32:\"93ef557d941247aea944f377d709d071\";a:10:{s:3:\"key\";s:32:\"93ef557d941247aea944f377d709d071\";s:10:\"product_id\";i:1745;s:12:\"variation_id\";i:3869;s:9:\"variation\";a:1:{s:15:\"attribute_level\";s:9:\"VIP Seats\";}s:8:\"quantity\";i:2;s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:50;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:50;s:8:\"line_tax\";i:0;}s:32:\"8b5700012be65c9da25f49408d959ca0\";a:10:{s:3:\"key\";s:32:\"8b5700012be65c9da25f49408d959ca0\";s:10:\"product_id\";i:1717;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:4;s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:72;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:72;s:8:\"line_tax\";i:0;}}}'),(20,2,'nickname','johndoe'),(21,2,'first_name',''),(22,2,'last_name',''),(23,2,'description',''),(24,2,'rich_editing','true'),(25,2,'syntax_highlighting','true'),(26,2,'comment_shortcuts','false'),(27,2,'admin_color','fresh'),(28,2,'use_ssl','0'),(29,2,'show_admin_bar_front','true'),(30,2,'locale',''),(31,2,'wp_capabilities','a:1:{s:10:\"subscriber\";b:1;}'),(32,2,'wp_user_level','0'),(33,2,'dismissed_wp_pointers',''),(34,1,'billing_first_name',''),(35,1,'billing_last_name',''),(36,1,'billing_company',''),(37,1,'billing_address_1',''),(38,1,'billing_address_2',''),(39,1,'billing_city',''),(40,1,'billing_postcode',''),(41,1,'billing_country',''),(42,1,'billing_state',''),(43,1,'billing_phone',''),(44,1,'billing_email','leafcolor.com@gmail.com'),(45,1,'shipping_first_name',''),(46,1,'shipping_last_name',''),(47,1,'shipping_company',''),(48,1,'shipping_address_1',''),(49,1,'shipping_address_2',''),(50,1,'shipping_city',''),(51,1,'shipping_postcode',''),(52,1,'shipping_country',''),(53,1,'shipping_state',''),(54,1,'last_update','1516958178'),(55,1,'closedpostboxes_post','a:0:{}'),(56,1,'metaboxhidden_post','a:6:{i:0;s:11:\"postexcerpt\";i:1;s:13:\"trackbacksdiv\";i:2;s:10:\"postcustom\";i:3;s:16:\"commentstatusdiv\";i:4;s:11:\"commentsdiv\";i:5;s:7:\"slugdiv\";}'),(57,1,'wp_product_import_error_log','a:0:{}'),(58,1,'nav_menu_recently_edited','226'),(59,1,'managenav-menuscolumnshidden','a:3:{i:0;s:15:\"title-attribute\";i:1;s:3:\"xfn\";i:2;s:11:\"description\";}'),(60,1,'metaboxhidden_nav-menus','a:7:{i:0;s:21:\"add-post-type-product\";i:1;s:27:\"add-post-type-iapp_showcase\";i:2;s:12:\"add-post_tag\";i:3;s:15:\"add-post_format\";i:4;s:15:\"add-product_cat\";i:5;s:15:\"add-product_tag\";i:6;s:20:\"add-tribe_events_cat\";}'),(61,1,'tribe_setDefaultNavMenuBoxes','1'),(66,1,'closedpostboxes_nav-menus','a:0:{}'),(67,1,'closedpostboxes_page','a:1:{i:0;s:12:\"revisionsdiv\";}'),(68,1,'metaboxhidden_page','a:4:{i:0;s:12:\"revisionsdiv\";i:1;s:16:\"commentstatusdiv\";i:2;s:7:\"slugdiv\";i:3;s:9:\"authordiv\";}'),(62,1,'wp_user-settings','editor=tinymce&libraryContent=browse&edit_element_vcUIPanelWidth=650&edit_element_vcUIPanelLeft=593px&edit_element_vcUIPanelTop=69px&hidetb=1'),(63,1,'wp_user-settings-time','1537126883'),(64,1,'closedpostboxes_dashboard','a:0:{}'),(65,1,'metaboxhidden_dashboard','a:6:{i:0;s:18:\"dashboard_activity\";i:1;s:22:\"tribe_dashboard_widget\";i:2;s:36:\"woocommerce_dashboard_recent_reviews\";i:3;s:28:\"woocommerce_dashboard_status\";i:4;s:21:\"dashboard_quick_press\";i:5;s:17:\"dashboard_primary\";}'),(71,1,'dismissed_update_notice','1'),(72,1,'dismissed_regenerating_thumbnails_notice','1');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-16 17:29:01
