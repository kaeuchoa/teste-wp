-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: teste_wp
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10)),
  KEY `woo_idx_comment_type` (`comment_type`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
INSERT INTO `wp_comments` VALUES (1,1,'A WordPress Commenter','wapuu@wordpress.example','https://wordpress.org/','','2018-01-10 05:50:52','2018-01-10 05:50:52','Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.',0,'post-trashed','','',0,0),(5,1148,'John Doe','example@example.org','http://example.org/','59.167.157.3','2012-09-03 10:18:04','2012-09-03 17:18:04','<h2>Headings</h2>\n<h1>Header one</h1>\n<h2>Header two</h2>\n<h3>Header three</h3>\n<h4>Header four</h4>\n<h5>Header five</h5>\n<h6>Header six</h6>\n<h2>Blockquotes</h2>\nSingle line blockquote:\n<blockquote>Stay hungry. Stay foolish.</blockquote>\nMulti line blockquote with a cite reference:\n<blockquote>People think focus means saying yes to the thing you\'ve got to focus on. But that\'s not what it means at all. It means saying no to the hundred other good ideas that there are. You have to pick carefully. I\'m actually as proud of the things we haven\'t done as the things I have done. Innovation is saying no to 1,000 things. <cite>Steve Jobs - Apple Worldwide Developers\' Conference, 1997</cite></blockquote>\n<h2>Tables</h2>\n<table>\n<tbody>\n<tr>\n<th>Employee</th>\n<th class=\"views\">Salary</th>\n<th></th>\n</tr>\n<tr class=\"odd\">\n<td><a href=\"http://example.org/\" rel=\"nofollow\">John Saddington</a></td>\n<td>$1</td>\n<td>Because that\'s all Steve Job\' needed for a salary.</td>\n</tr>\n<tr class=\"even\">\n<td><a href=\"http://example.org/\" rel=\"nofollow\">Tom McFarlin</a></td>\n<td>$100K</td>\n<td>For all the blogging he does.</td>\n</tr>\n<tr class=\"odd\">\n<td><a href=\"http://example.org/\" rel=\"nofollow\">Jared Erickson</a></td>\n<td>$100M</td>\n<td>Pictures are worth a thousand words, right? So Tom x 1,000.</td>\n</tr>\n<tr class=\"even\">\n<td><a href=\"http://example.org/\" rel=\"nofollow\">Chris Ames</a></td>\n<td>$100B</td>\n<td>With hair like that?! Enough said...</td>\n</tr>\n</tbody>\n</table>\n<h2>Definition Lists</h2>\n<dl><dt>Definition List Title</dt><dd>Definition list division.</dd><dt>Startup</dt><dd>A startup company or startup is a company or temporary organization designed to search for a repeatable and scalable business model.</dd><dt>#dowork</dt><dd>Coined by Rob Dyrdek and his personal body guard Christopher \"Big Black\" Boykins, \"Do Work\" works as a self motivator, to motivating your friends.</dd><dt>Do It Live</dt><dd>I\'ll let Bill O\'Reilly will <a title=\"We\'ll Do It Live\" href=\"https://www.youtube.com/watch?v=O_HyZ5aW76c\" rel=\"nofollow\">explain</a> this one.</dd></dl>\n<h2>Unordered Lists (Nested)</h2>\n<ul>\n	<li>List item one\n<ul>\n	<li>List item one\n<ul>\n	<li>List item one</li>\n	<li>List item two</li>\n	<li>List item three</li>\n	<li>List item four</li>\n</ul>\n</li>\n	<li>List item two</li>\n	<li>List item three</li>\n	<li>List item four</li>\n</ul>\n</li>\n	<li>List item two</li>\n	<li>List item three</li>\n	<li>List item four</li>\n</ul>\n<h2>Ordered List (Nested)</h2>\n<ol>\n	<li>List item one\n<ol>\n	<li>List item one\n<ol>\n	<li>List item one</li>\n	<li>List item two</li>\n	<li>List item three</li>\n	<li>List item four</li>\n</ol>\n</li>\n	<li>List item two</li>\n	<li>List item three</li>\n	<li>List item four</li>\n</ol>\n</li>\n	<li>List item two</li>\n	<li>List item three</li>\n	<li>List item four</li>\n</ol>\n<h2>HTML Tags</h2>\nThese supported tags come from the WordPress.com code <a title=\"Code\" href=\"http://en.support.wordpress.com/code/\" rel=\"nofollow\">FAQ</a>.\n\n<strong>Address Tag</strong>\n\n<address>1 Infinite Loop\nCupertino, CA 95014\nUnited States</address><strong>Anchor Tag (aka. Link)</strong>\n\nThis is an example of a <a title=\"Apple\" href=\"http://apple.com\" rel=\"nofollow\">link</a>.\n\n<strong>Abbreviation Tag</strong>\n\nThe abbreviation <abbr title=\"Seriously\">srsly</abbr> stands for \"seriously\".\n\n<strong>Acronym Tag</strong>\n\nThe acronym <acronym title=\"For The Win\">ftw</acronym> stands for \"for the win\".\n\n<strong>Big Tag</strong>\n\nThese tests are a <big>big</big> deal, but this tag is no longer supported in HTML5.\n\n<strong>Cite Tag</strong>\n\n\"Code is poetry.\" --<cite>Automattic</cite>\n\n<strong>Code Tag</strong>\n\nYou will learn later on in these tests that <code>word-wrap: break-word;</code> will be your best friend.\n\n<strong>Delete Tag</strong>\n\nThis tag will let you <del>strikeout text</del>, but this tag is no longer supported in HTML5 (use the <code>&lt;strike&gt;</code> instead).\n\n<strong>Emphasize Tag</strong>\n\nThe emphasize tag should <em>italicize</em> text.\n\n<strong>Insert Tag</strong>\n\nThis tag should denote <ins>inserted</ins> text.\n\n<strong>Keyboard Tag</strong>\n\nThis scarcely known tag emulates <kbd>keyboard text</kbd>, which is usually styled like the <code>&lt;code&gt;</code> tag.\n\n<strong>Preformatted Tag</strong>\n\nThis tag styles large blocks of code.\n<pre>.post-title {\n  margin: 0 0 5px;\n  font-weight: bold;\n  font-size: 38px;\n  line-height: 1.2;\n}</pre>\n\n<strong>Quote Tag</strong>\n\n<q>Developers, developers, developers...</q> --Steve Ballmer\n\n<strong>Strong Tag</strong>\n\nThis tag shows <strong>bold<strong> text.</strong></strong>\n\n<strong>Subscript Tag</strong>\n\nGetting our science styling on with H<sub>2</sub>O, which should push the \"2\" down.\n\n<strong>Superscript Tag</strong>\n\nStill sticking with science and Isaac Newton\'s E = MC<sup>2</sup>, which should lift the 2 up.\n\n<strong>Teletype Tag</strong>\n\nThis rarely used tag emulates <tt>teletype text</tt>, which is usually styled like the <code>&lt;code&gt;</code> tag.\n\n<strong>Variable Tag</strong>\n\nThis allows you to denote <var>variables</var>.',0,'1','','',0,0),(6,1148,'Anonymous User','fake@email.com','','67.3.69.40','2013-03-11 23:45:54','2013-03-12 04:45:54','This user it trying to be anonymous.\n\n\n    They used a fake email, so there should be no <a href=\"http://gravatar.com/\" title=\"Gravatar\" rel=\"nofollow\">Gravatar</a> associated with it.\n    They did not speify a website, so there should be no link to it in the comment.\n',0,'1','','',0,0),(7,1148,'Jane Doe','example@example.org','http://example.org/','204.54.106.1','2013-03-12 13:17:35','2013-03-12 20:17:35','Comments? I love comments!',0,'1','','',0,0),(8,1148,'John Doe','example@example.org','http://example.org','24.126.245.62','2013-03-14 07:53:26','2013-03-14 14:53:26','These tests are amazing!',0,'1','','',0,0),(9,1148,'John Doe','example@example.org','http://example.org/','24.126.245.62','2013-03-14 07:56:46','2013-03-14 14:56:46','Author Comment.',0,'1','','',0,0),(10,1148,'John Doe','example@example.org','http://example.org/','24.126.245.62','2013-03-14 07:57:01','2013-03-14 14:57:01','Comment Depth 01',0,'1','','',0,0),(11,1148,'Jane Bloggs','example@example.org','http://example.org/','24.126.245.62','2013-03-14 08:01:21','2013-03-14 15:01:21','Comment Depth 02',0,'1','','',10,0),(12,1148,'Fred Bloggs','example@example.org','http://example.org/','24.126.245.62','2013-03-14 08:02:06','2013-03-14 15:02:06','Comment Depth 03',0,'1','','',11,0),(13,1148,'Fred Bloggs','example@example.org','http://example.org/','24.126.245.62','2013-03-14 08:03:22','2013-03-14 15:03:22','Comment Depth 04',0,'1','','',12,0),(14,1148,'Joe Bloggs','example@example.org','http://example.org/','24.126.245.62','2013-03-14 08:10:29','2013-03-14 15:10:29','Comment Depth 05\n\nAlso an author comment.',0,'1','','',13,0),(15,1148,'Jane Bloggs','example@example.org','http://example.org/','24.126.245.62','2013-03-14 08:12:16','2013-03-14 15:12:16','Comment Depth 06',0,'1','','',14,0),(16,1148,'Joe Bloggs','example@example.org','http://example.org/','24.126.245.62','2013-03-14 08:12:58','2013-03-14 15:12:58','Comment Depth 07',0,'1','','',15,0),(17,1148,'Jane Bloggs','example@example.org','http://example.org/','24.126.245.62','2013-03-14 08:13:42','2013-03-14 15:13:42','Comment Depth 08',0,'1','','',16,0),(18,1148,'Joe Bloggs','example@example.org','http://example.org/','24.126.245.62','2013-03-14 08:14:13','2013-03-14 15:14:13','Comment Depth 09',0,'1','','',17,0),(19,1148,'John Doe','example@example.org','http://example.org/','24.126.245.62','2013-03-14 08:14:47','2013-03-14 15:14:47','Comment Depth 10\n\nAlso an author comment.',0,'1','','',18,0),(20,1148,'Jane Doe','example@example.org','http://example.org/','24.126.245.62','2013-03-14 09:56:43','2013-03-14 16:56:43','Image comment.\n\n',0,'1','','',0,0),(21,1148,'John Doe','example@example.org','http://example.org/','24.126.245.62','2013-03-14 11:23:24','2013-03-14 18:23:24','We are totally going to blog about these tests!',0,'1','','',0,0),(22,1148,'John Doe','example@example.org','http://example.org/','24.126.245.62','2013-03-14 11:27:54','2013-03-14 18:27:54','We use these tests all the time! Killer stuff!',0,'1','','',0,0),(23,1148,'Jane Doe','example@example.org','http://example.org/','24.126.245.62','2013-03-14 11:30:33','2013-03-14 18:30:33','Thanks for all the comments, everyone!',0,'1','','',0,0),(39,1,'Steve Han','leafcolor.com@gmail.com','','116.104.239.92','2018-02-22 23:08:37','2018-02-22 16:08:37','Thanks for your comment',0,'post-trashed','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_2) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0.2 Safari/604.4.7','',0,1);
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-16 17:28:57
