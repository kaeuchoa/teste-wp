-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: teste_wp
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=227 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_taxonomy`
--

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` VALUES (1,1,'category','',0,0),(2,2,'product_type','',0,19),(3,3,'product_type','',0,1),(4,4,'product_type','',0,3),(5,5,'product_type','',0,1),(6,6,'product_visibility','',0,0),(7,7,'product_visibility','',0,0),(8,8,'product_visibility','',0,0),(9,9,'product_visibility','',0,0),(10,10,'product_visibility','',0,0),(11,11,'product_visibility','',0,0),(12,12,'product_visibility','',0,0),(13,13,'product_visibility','',0,0),(14,14,'product_visibility','',0,0),(32,32,'category','Posts that have edge-case related tests',0,0),(42,42,'category','Posts in this category test markup tags and styles.',0,0),(43,43,'category','Posts that have media-related tests',0,0),(51,51,'category','Posts in this category test post formats.',0,0),(62,62,'category','Posts with template-related tests',0,0),(223,223,'post_tag','',0,0),(224,224,'post_tag','',0,0),(80,80,'post_tag','',0,0),(83,83,'post_tag','',0,0),(109,109,'post_tag','',0,0),(112,112,'post_tag','',0,0),(118,118,'post_tag','',0,0),(119,119,'post_tag','',0,0),(129,129,'post_tag','',0,0),(142,142,'post_tag','',0,0),(147,147,'post_tag','',0,0),(159,159,'post_tag','',0,0),(186,186,'post_tag','Tags posts about WordPress.',0,0),(189,189,'nav_menu','',0,18),(190,190,'nav_menu','',0,3),(191,191,'nav_menu','',0,0),(192,192,'nav_menu','',0,20),(193,193,'nav_menu','',0,0),(194,194,'post_format','',0,0),(195,195,'post_format','',0,0),(196,196,'post_format','',0,0),(197,197,'post_format','',0,0),(198,198,'post_format','',0,0),(199,199,'post_format','',0,0),(200,200,'post_format','',0,0),(201,201,'post_format','',0,0),(202,202,'post_format','',0,0),(225,225,'product_cat','',0,0),(203,203,'product_cat','',0,7),(204,204,'product_cat','',203,3),(205,205,'product_cat','',203,4),(206,206,'product_cat','',0,5),(207,207,'product_cat','',0,12),(208,208,'product_cat','',207,6),(209,209,'product_cat','',207,6),(213,213,'tribe_events_cat','',0,0),(210,210,'pa_color','',0,2),(211,211,'pa_color','',0,1),(212,212,'pa_color','',0,1),(214,214,'tribe_events_cat','',0,0),(215,215,'tribe_events_cat','',0,1),(216,216,'tribe_events_cat','',0,0),(217,217,'tribe_events_cat','',0,1),(226,226,'nav_menu','',0,5);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-17 22:08:50
