-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: teste_wp
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=227 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'News','news',0),(2,'simple','simple',0),(3,'grouped','grouped',0),(4,'variable','variable',0),(5,'external','external',0),(6,'exclude-from-search','exclude-from-search',0),(7,'exclude-from-catalog','exclude-from-catalog',0),(8,'featured','featured',0),(9,'outofstock','outofstock',0),(10,'rated-1','rated-1',0),(11,'rated-2','rated-2',0),(12,'rated-3','rated-3',0),(13,'rated-4','rated-4',0),(14,'rated-5','rated-5',0),(32,'Culture','culture',0),(42,'Economy','economy',0),(43,'Media','media-2',0),(51,'Conference','conference',0),(62,'Technology','technology',0),(223,'Meeting','meeting',0),(80,'Ticket','ticket',0),(83,'Media','media',0),(109,'Hall','hall',0),(112,'Gallery','gallery',0),(118,'Coder','coder',0),(119,'Event Theme','event-theme',0),(129,'Designer','designer',0),(225,'Uncategorized','uncategorized',0),(224,'Games','games',0),(142,'Pictures','pictures',0),(147,'Event Ticket','event-ticket',0),(159,'Speaker','speaker',0),(186,'WordPress','wordpress',0),(189,'All Pages','all-pages',0),(190,'Short','short',0),(191,'All Pages Flat','all-pages-flat',0),(192,'Testing Menu','testing-menu',0),(193,'Empty Menu','empty-menu',0),(194,'Gallery','post-format-gallery',0),(195,'Aside','post-format-aside',0),(196,'Chat','post-format-chat',0),(197,'Link','post-format-link',0),(198,'Image','post-format-image',0),(199,'Quote','post-format-quote',0),(200,'Status','post-format-status',0),(201,'Video','post-format-video',0),(202,'Audio','post-format-audio',0),(203,'Music','music',0),(204,'Singles','singles',0),(205,'Albums','albums',0),(206,'Posters','posters',0),(207,'Clothing','clothing',0),(208,'Hoodies','hoodies',0),(209,'T-shirts','t-shirts',0),(210,'Black','black',0),(211,'Blue','blue',0),(212,'Green','green',0),(213,'Startup','startup',0),(214,'Mobile','mobile',0),(215,'Technology','technology',0),(216,'Fintech','fintech',0),(217,'Expo','expo',0),(226,'menu_principal','menu_principal',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-17 22:08:52
